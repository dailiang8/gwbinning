# gwbinning

############################################
I. General description
############################################

This Python tutorial code demonstrates the technique of relative binning for efficient evaluation
of the likelihood function for gravitational wave data (See Zackay, Dai, Venumadhav 2018, https://arxiv.org/abs/1806.08792 for theoretical description of the method). Using GW170817 as an example, the tutorial shows a sample code to construct frequency-binned summary data, and compute the likelihood function using the summary data. 

As an application, we also provide a sample code to find the best-fit waveform parameters by numerically maximizing the likelihood.


############################################
II. Required python packages
############################################

This tutorial code only requires the common python packages numpy and scipy. For demonstration, 
the analytical TaylorF2 waveform (including the effects of aligned spins and tidal deformation) 
is implemented as a python function. In order to make the tutorial as accessible as possible, we do not require the LIGO data analysis package LALSuite to be installed.


############################################
III. Description of Python code files
############################################

	waveform.py: This analytically implements the TaylorF2 waveform model, as well as other functions
				 to be used in the tutorial

	binning.py: This contains the algorithms to construct frequency bins and to generate the summary data

	GW170817_binning.py: This is the Python tutorial file to apply the binning technique to the case of GW170817. 


############################################
IV. LIGO data
############################################

The user has to download the strain data for GW170817 from the LIGO website:

	https://losc.ligo.org/events/GW170817/
	
In the example we give, we use T = 2048 sec chunk of data at a sampling rate of 4096Hz after noise subtraction:

	https://dcc.ligo.org/public/0146/P1700349/001/H-H1_LOSC_CLN_4_V1-1187007040-2048.txt.gz
	https://dcc.ligo.org/public/0146/P1700349/001/L-L1_LOSC_CLN_4_V1-1187007040-2048.txt.gz


############################################
V. Posterior samples for GW170817
############################################

We generate posterior samples for GW170817 utilizing the relative binning technique. We provide posterior samples
based on two different waveform models from LAL: IMRPhenomD_NRTidal and TaylorF2.

The samples are provided in terms of a set of 8 parameters. The columns are:
	
	     Mc:  detector-frame chirp mass [Msun]
	    eta:  symmetric mass ratio
	    s1z:  aligned spin component for the primary
	    s2z:  aligned spin component for the secondary
	lambda1:  tidal deformability for the primary
	lambda2:  tidal deformability for the secondary
	    tc1:  merger time for the primary [sec]
	    tc2:  merger time for the secondary [sec]
		
Posterior sample files:

	GW170817_IMRPhenomDNRTidal_emcee.txt      ------  [IMRPhenomD_NRTidal; using emcee]
	GW170817_IMRPhenomDNRTidal_MultiNest.txt  ------  [IMRPhenomD_NRTidal; using pyMultiNest]
	GW170817_TaylorF2_MultiNest.txt           ------  [TaylorF2; using pyMultiNest]
